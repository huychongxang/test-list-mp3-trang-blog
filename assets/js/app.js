$(document).ready(function() {
    'use strict';

    var $nav = $("nav");
    var width_screen = $(window).width();
    if (width_screen > 1024) {
        // fullpage customization   
        $("#fullpage").fullpage({
            sectionSelector: ".vertical-scrolling",
            navigationPosition: 'left',
            scrollingSpeed: 1300,
            css3: true,
            licenseKey: "367adebc-eb28e4ea-b321694c-6240c516-7ffc06c2",
            easingcss3: "cubic-bezier(0.77, 0, 0.175, 1)",
            navigation: true,
            lockAnchors: true,
            fitToSection: false,
            scrollOverflowOptions: {
            probeType: 3
            },
            loopBottom: false,
            anchors: 
            [ 
                "firstSection", "secondSection", "thirdSection", "fourthSection", "fifthSection", "sixthSection"
            ],
            responsiveWidth: 1100,
            afterLoad: function(anchorLink, index) {
                if (index == 4) {
                    $("#fp-nav").hide(); 
                }
            }, 
            onLeave: function(index, nextIndex, direction) {
                if (index == 4) {
                    $("#fp-nav").show();
                }
            },
            afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {  
                if (anchorLink == "sixthSection" && slideIndex == 1) {
                    $.fn.fullpage.setAllowScrolling(false, "up");
                    $nav.css("background", "#232323");
                }
            },
            onSlideLeave: function(anchorLink, index, slideIndex, direction) {
                if (anchorLink == "sixthSection" && slideIndex == 1) {
                    $.fn.fullpage.setAllowScrolling(true, "up");
                }
            }
        }); 
    } else {
        $.fn.fullpage.destroy('all');
    }

    // if ($(window).width() < 769) {
    //   $("#motframe").removeClass("fp-auto-height");
    //   // mobileSwipe()
    // } else {
    //   $("#motframe").addClass("fp-auto-height");
    // }

    $("#fp-nav ul li:first-child span:nth-child(2)").append(
        "<p> navy beat</p>"
    );
    $("#fp-nav ul li:first-child a").addClass('active');
    $("#fp-nav ul li:nth-child(2) span:nth-child(2)").append(
        "<p> các dự án đã thực hiện</p>"
    );
    $("#fp-nav ul li:nth-child(3) span:nth-child(2)").append(
        "<p> vì sao nên chọn navy beat?</p>"
    );
    $("#fp-nav ul li:nth-child(4) span:nth-child(2)").append(
        "<p> sound branding</p>"
    );
    $("#fp-nav ul li:nth-child(5) span:nth-child(2)").append(
        "<p> music marketing</p>"
    );
    $("#fp-nav ul li:last-child span:nth-child(2)").append(
        "<p> Liên hệ</p>"
    );
    

    $('.fadeInUp').css({
        'animation-name' : 'fadeInUp',
        'visibility' : 'visible'
    });


    // effect page wild animals

    $('.img-section').each(function(){

        var $this = $(this),
            thisTopPos = $this.offset().top,
            thisBottomPos = thisTopPos + $this.height(),
            scrollPos = window.pageYOffset,
            $thisCopyBlock = $this.find('.img-section__copy'),
            power = 10;

        function translateBox(el){
            
            scrollPos = window.pageYOffset;
            if(scrollPos + window.innerHeight >= thisTopPos && scrollPos <= thisBottomPos) {
                if(el.hasClass('img-section--right')){
                    $thisCopyBlock.css('transform', 'translateX(-' +  Math.round( ((scrollPos + $(window).height()) - thisTopPos) / power) + 'px)');
                } else {
                    $thisCopyBlock.css('transform', 'translateX(' + Math.round( ((scrollPos + $(window).height()) - thisTopPos) / power) + 'px)');
                }
                
            };
        }

        function pushNextContent(el){
            
            var thisHeight = el.innerHeight(),
                thisCopy = el.find('.img-section__copy'),
                copyHeight = thisCopy.innerHeight(),
                paddingTop = (copyHeight - thisHeight) + 110,
                copyIsBiggerThanParent = copyHeight > thisHeight,
                nextBox = el.next('.regular-section'),
                previousBox = el.prev('.regular-section');

            if(copyIsBiggerThanParent && el.hasClass('img-section--right')) {

                //var nextBox = el.next();
                
                nextBox.css('padding-top', paddingTop);
                thisCopy.css({
                    'top': '50px',
                    'bottom': 'auto'
                });
                el.css('margin', '0');

            } else if (copyIsBiggerThanParent && el.hasClass('img-section--left')) {

                //var previousBox = el.prev();

                previousBox.css('padding-bottom', paddingTop);
                thisCopy.css({
                    'top': 'auto',
                    'bottom': '50px'
                });
                el.css('margin', '0');

            } else if (el.hasClass('img-section--left') && el.prev('.regular-section').length && window.innerWidth >= 992) {
                el.prev('.regular-section').css('padding-bottom', '110px');
                el.css('margin-top', '0px');
            } else {
                nextBox.css('padding-top', '');
                previousBox.css('padding-bottom', '');
                el.css('margin', '');
                thisCopy.css({
                    'top': '',
                    'bottom': ''
                });
            }

            thisCopy.addClass('img-section__copy--visible')
        
        };

        $( window ).scroll(function() { 
            translateBox($this);
        });

        $( window ).resize(function() { 
            setTimeout(function(){ pushNextContent($this); }, 500);
        });

        translateBox($this);
        
        setTimeout(function() { 
            pushNextContent($this); 
        }, 200);

    });

    function enlargeImg() {
        $('.img-section').each(function(){
            var $this = $(this),
                thisTopPos = $(this).offset().top,
                thisBottomPos = $(this).offset().top + $(this).height(),
                scrollPos = window.pageYOffset;
            if(scrollPos + window.innerHeight/1.2 > thisTopPos){
                $this.addClass('img-section--active')
            }       
        });
    };

});
