<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sản phẩm tương tự</title>
    <!-- Link Css -->
    <link rel="shortcut icon" href="assets/images/logo-navy.png">
    <link rel="stylesheet" type="text/css" href="assets/css/reset.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/font-awesome/css/fontawesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/font-awesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/fullpage.min.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>

<body>
<!-- =========== HEADER =========== -->
<header id="header">
    <nav class="navbar navbar-expand-lg" id="menu-ngang">
        <div class="container-fluid">
            <a class="navbar-brand landing-logo wow fadeInUp" data-wow-delay=".25s" data-wow-offset="10"
               data-wow-duration="3s" href="#">
                <img src="assets/images/logo-navy.png" class="img-fluid" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#landing-menu"
                    aria-controls="landing-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">&#9776;</span>
            </button>
            <div class="collapse navbar-collapse wow fadeInUp" data-wow-delay=".25s" data-wow-offset="10"
                 data-wow-duration="3s" id="landing-menu">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Các dự án đã thực hiện</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- =========== /HEADER =========== -->
<!-- Ảnh banner header -->
<img src="assets/images/banner-header-cac-du-an.png" class="img-fluid banner-cacduan">
<!-- /Ảnh banner header -->
<!-- Khối các dự án đã thực hiện -->
<div class="container text-center projects__title projects__same">
    <h2 class="">Sản phẩm tương tự</h2>
</div>
<div class="container" id="projects__content">
    <!-- Category tabs -->
    <div class="row">
        <div class="col-sm-12">
            <nav>
                <ul class="nav tieude-tab" role="tablist" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#sanpham-all" role="tab">
                                <span>
                                    Hiển thị tất cả
                                </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#music-page" role="tab">
                                <span>
                                    music marketing
                                </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#sound-page" role="tab">
                                <span>
                                    sound logo
                                </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#brand-page" role="tab">
                                <span>
                                    brand voice
                                </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#song-page" role="tab">
                                <span>
                                    brand song
                                </span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <!-- /Category tabs -->
    <!-- Category contents -->
    <div class="row">
        <div class="col-sm-12">
            <div class="tab-content" id="myTabContent">
                <!-- All -->
                <div id="sanpham-all" class="tab-pane fade show active" role="tabpanel" aria-labelledby="all-tab">
                    <div class="row" id="landing-products">
                        <div class="col-sm-6">
                            <div class="pics block-list-mp3" id="a1">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="mp3-icon">
                                            <i id="play-button"
                                               class="play-button material-icons play-pause text-primary far fa-play-circle"
                                               aria-hidden="true"></i>
                                            <i id="pause-button"
                                               class="pause-button material-icons play-pause hidden text-primary far fa-pause-circle"
                                               aria-hidden="true"></i>
                                            <div class="col">
                                                <img id="thumbnail" class="img-fluid" src="" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sound-wrapper">
                                        <div class="sound-wave">
                                            <canvas id="a1"></canvas>
                                        </div>
                                        <ul class="playlist list-group list-group-flush">
                                            <li audio_url="test.mp3" img_url="https://lorempixel.com/400/400/cats/4"
                                                class="active list-group-item playlist-item">
                                                Bài 1
                                            </li>
                                            <li audio_url="2.mp3" class="list-group-item playlist-item"
                                                img_url="https://lorempixel.com/400/400/cats/3">
                                                Bài 2
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <article class="products__title">
                                    <h4> Top 100 RnB</h4>
                                    <p>
                                        Xin chân thành cảm ơn quý công ty đã tin tưởng và đồng
                                        hành cùng Navy Beat trong các hoạt động truyền thông
                                    </p>
                                </article>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="pics block-list-mp3" id="a2">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="mp3-icon">
                                            <i id="play-button"
                                               class="play-button material-icons play-pause text-primary far fa-play-circle"
                                               aria-hidden="true"></i>
                                            <i id="pause-button"
                                               class="pause-button material-icons play-pause hidden text-primary far fa-pause-circle"
                                               aria-hidden="true"></i>
                                            <div class="col">
                                                <img id="thumbnail" class="img-fluid" src="" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sound-wrapper">
                                        <div class="sound-wave">
                                            <canvas id="a2"></canvas>
                                        </div>
                                        <ul class="playlist list-group list-group-flush">
                                            <li audio_url="test.mp3" img_url="https://lorempixel.com/400/400/cats/1"
                                                class="active list-group-item playlist-item">
                                                Bài 1
                                            </li>
                                            <li audio_url="2.mp3" class="list-group-item playlist-item"
                                                img_url="https://lorempixel.com/400/400/cats/4">
                                                Bài 2
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <article class="products__title">
                                    <h4> Top 100 RnB</h4>
                                    <p>
                                        Xin chân thành cảm ơn quý công ty đã tin tưởng và đồng
                                        hành cùng Navy Beat trong các hoạt động truyền thông
                                    </p>
                                </article>
                            </div>
                        </div>
                        <!-- Phân trang -->
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination">
                                            <li class="page-item">
                                                <a class="page-link" href="#" aria-label="Previous" title="Prev">
                                                    &lt;</a>
                                            </li>
                                            <li class="page-item active">
                                                <span>1</span>
                                            </li>
                                            <li class="page-item">
                                                <a class="page-link" href="#">
                                                    <span>2</span>
                                                </a>
                                            </li>
                                            <li class="page-item">
                                                <a class="page-link" href="#">
                                                    <span>3</span>
                                                </a>
                                            </li>
                                            <li class="page-item">
                                                <a class="page-link" href="#" aria-label="Next" title="Next"> &gt; </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <!-- /Phân trang  -->
                    </div>
                </div>
                <!-- /All -->
            </div>
        </div>
    </div>
    <!-- /Category contents -->
</div>
<!-- /Khối các dự án đã thực hiện -->
<!-- Footer -->
<div class="project__footer">
    <img src="assets/images/navy-beat-footer.png" class="img-fluid" alt="">
    <span class="footer-contact">
            <a href="" title="">
                <img src="assets/images/fb-icon.png" class="img-fluid">
            </a>
            <a href="" title="">
                <img src="assets/images/utube-icon.png" class="img-fluid">
            </a>
        </span>
</div>
<!-- /Footer -->
<!-- Link Script -->
<script src="assets/vendor/jquery-3.3.1.js"></script>
<script src="assets/vendor/wow.min.js"></script>
<script src="assets/vendor/fullpage.min.js"></script>
<script src="assets/vendor/fullpage.extensions.min.js"></script>
<script src="assets/vendor/bootstrap.js"></script>
<script src="assets/vendor/popper.min.js"></script>
<script src="assets/vendor/font-awesome/js/fontawesome.min.js"></script>
<script src="assets/js/app.js"></script>
<script src="assets/vendor/jquery-ui-slider.js"></script>
<script type="text/javascript">
    if ($(window).width() > 991) {
        new WOW().init();
    } else {
        $(".wow").removeClass("wow");
    }
</script>
<script type="text/javascript" src="main.js"></script>
</body>

</html>