jQuery(document).ready(function ($) {
    /***** Set thumbnail image từng khối list mp3 *****/
    var activeLies = $('ul.playlist li.active');
    $.each(activeLies, function (key, value) {
        var currentImage = $(value).attr('img_url');
        var img = $(value).closest('.block-list-mp3').find('img');
        changeImageListMp3(img, currentImage);
    });
    /**** Create object audio  ****/
    var listMp3Blocks = $('.block-list-mp3');
    var objectAudios = {};
    var objectMedia  = {};
    $.each(listMp3Blocks, function (index, value) {
        var id = $(value).attr('id');
        var audio = new Audio();
        objectAudios[id] = audio;
    });

    /**** Event khi click song ******/
    $('ul.playlist li').click(function (event) {
        event.preventDefault();
        if ($(this).hasClass('active')) {
            var rootDiv = $(this).closest('div.pics');
            var rootId = rootDiv.attr('id');
            // Change thumbnail
            var currentImage = $(this).attr('img_url');
            var img = rootDiv.find('img');
            changeImageListMp3(img, currentImage);
            // Toggle Play/Pause Button
            rootDiv.find('.play-button').addClass('hidden');
            rootDiv.find('.pause-button').removeClass('hidden');

            var srcMusic = $(this).attr('audio_url');
            playMusic(rootId,srcMusic);
        } else {
            $(this).siblings('li.active').removeClass('active');
            $(this).addClass('active');

            var rootDiv = $(this).closest('div.pics');
            var rootId = rootDiv.attr('id');
            // Change thumbnail
            var currentImage = $(this).attr('img_url');
            var img = rootDiv.find('img');
            changeImageListMp3(img, currentImage);
            // Toggle Play/Pause Button
            rootDiv.find('.play-button').addClass('hidden');
            rootDiv.find('.pause-button').removeClass('hidden');

            var srcMusic = $(this).attr('audio_url');
            playMusic(rootId,srcMusic);
        }
    });

    /*** Click start or pause ****/
    $('#landing-products').on('click', '.play-button', function (event) {
        event.preventDefault();
        var rootDiv = $(this).closest('div.pics');
        var rootId = rootDiv.attr('id');
        $(this).addClass('hidden');
        rootDiv.find('#pause-button').removeClass('hidden');

        objectAudios[rootId].play();
    });
    $('#landing-products').on('click', '.pause-button', function (event) {
        event.preventDefault();
        var rootDiv = $(this).closest('div.pics');
        var rootId = rootDiv.attr('id');
        $(this).addClass('hidden');
        rootDiv.find('#play-button').removeClass('hidden');

        objectAudios[rootId].pause();
    });

    /****** Functions *******/
    function changeImageListMp3(img, srcImage) {
        img.attr('src',srcImage);
    }

    function playMusic(rootId, srcMusic) {
        var audio = objectAudios[rootId];
        if(audio.src != undefined){
            audio.pause();
            audio.src = srcMusic;
        } else {
            audio.src = srcMusic;
        }
        audio.crossOrigin = 'anonymous';
        audio.load(); // update new source
        audio.controls = false;
        // audio.loop = true;
        audio.autoplay = true;
        var context = new AudioContext();
        var src = context.createMediaElementSource(audio);
        objectMedia[rootId] = src;
        var analyser = context.createAnalyser();
        var canvas = document.querySelector('canvas#'+rootId);
        // canvas.width = window.innerWidth;
        // canvas.height = window.innerHeight;
        canvas.width = 1000;
        canvas.height = 250;
        var ctx = canvas.getContext("2d");

        src.connect(analyser);
        analyser.connect(context.destination);

        analyser.fftSize = 256;

        var bufferLength = analyser.frequencyBinCount;

        var dataArray = new Uint8Array(bufferLength);

        var WIDTH = canvas.width;
        var HEIGHT = canvas.height;

        var barWidth = (WIDTH / bufferLength) * 2.5;
        var barHeight;
        var x = 0;

        function renderFrame() {
            requestAnimationFrame(renderFrame);

            x = 0;

            analyser.getByteFrequencyData(dataArray);

            ctx.fillStyle = "#000";
            ctx.fillRect(0, 0, WIDTH, HEIGHT);

            for (var i = 0; i < bufferLength; i++) {
                barHeight = dataArray[i];

                var r = barHeight + (25 * (i / bufferLength));
                var g = 250 * (i / bufferLength);
                var b = 50;

                ctx.fillStyle = "rgb(" + r + "," + g + "," + b + ")";
                ctx.fillRect(x, HEIGHT - barHeight, barWidth, barHeight);

                x += barWidth + 1;
            }
        }
        audio.play();
        renderFrame();
    };
});