jQuery(document).ready(function ($) {
    // Set ảnh thumb lúc khởi tạo ban đầu cho mỗi khối list music
    var activeLies = $('ul.playlist li.active');
    $.each(activeLies, function (key, value) {
        var currentImage = $(value).attr('img_url');
        // var img = $(value).closest('.sound-wrapper').siblings('.card-body').find('img').attr('src',currentImage);
        changeImageListMp3(value, currentImage);
    });
    // End Set ảnh thumb lúc khởi tạo ban đầu cho mỗi khối list music

    // Audio
    // for each từng div sm-6 mà có nội dung là mp3 list
    var audio = new Audio();
    var playButton = $('.play-button');
    var pauseButton = $('.pause-button');
    var context;
    var src;

    // End Audio

    /****** Events ******/
    function changeImageListMp3(object, image) {
        $(object).closest('.sound-wrapper').siblings('.card-body').find('img').attr('src', image);
    }

    // Click li list nhạc
    $('ul.playlist li').click(function (event) {
        if (!$(this).hasClass('active')) {
            $(this).siblings('ul.playlist li.active').removeClass('active');
            $(this).addClass('active');
            $(this).closest('.sound-wrapper').siblings('.card-body').find('.play-button').addClass('hidden');
            $(this).closest('.sound-wrapper').siblings('.card-body').find('.pause-button').removeClass('hidden');
            src = $(this).attr('audio_url');
            var thisLi = $(this);
            var image = $(this).attr('img_url');
            var canvasId = $(this).closest('.sound-wrapper').find('canvas').attr('id');
            console.log(canvasId);
            changeImageListMp3($(this), image);
            playMusic(canvasId, src);
        } else {
            $(this).closest('.sound-wrapper').siblings('.card-body').find('.play-button').addClass('hidden');
            $(this).closest('.sound-wrapper').siblings('.card-body').find('.pause-button').removeClass('hidden');
            src = $(this).attr('audio_url');
            playMusic(src);
        }

    });
    playButton.click(function (event) {
        event.preventDefault();

        //chekc xem hiện browsẻ có đang chơi bản nào không, có thì dừng luôn
        $(this).addClass('hidden');
        $(this).siblings('.pause-button').removeClass('hidden');
        if (audio.src) {
            audio.play();
        }
    });
    pauseButton.click(function (event) {
        event.preventDefault();
        $(this).addClass('hidden');
        $(this).siblings('.play-button').removeClass('hidden');
        audio.pause();
    });

    /****** End Events ******/

    function playMusic(canvasId, src) {
        audio.crossOrigin = 'anonymous';
        audio.src = src;
        audio.load(); // update new source
        audio.controls = false;
        // audio.loop = true;
        audio.autoplay = true;
        context = new AudioContext();
        src = context.createMediaElementSource(audio);
        var analyser = context.createAnalyser();

        var canvas = document.getElementById(canvasId);
        // canvas.width = window.innerWidth;
        // canvas.height = window.innerHeight;
        canvas.width = 1000;
        canvas.height = 250;
        var ctx = canvas.getContext("2d");

        src.connect(analyser);
        analyser.connect(context.destination);

        analyser.fftSize = 256;

        var bufferLength = analyser.frequencyBinCount;

        var dataArray = new Uint8Array(bufferLength);

        var WIDTH = canvas.width;
        var HEIGHT = canvas.height;

        var barWidth = (WIDTH / bufferLength) * 2.5;
        var barHeight;
        var x = 0;

        function renderFrame() {
            requestAnimationFrame(renderFrame);

            x = 0;

            analyser.getByteFrequencyData(dataArray);

            ctx.fillStyle = "#000";
            ctx.fillRect(0, 0, WIDTH, HEIGHT);

            for (var i = 0; i < bufferLength; i++) {
                barHeight = dataArray[i];

                var r = barHeight + (25 * (i / bufferLength));
                var g = 250 * (i / bufferLength);
                var b = 50;

                ctx.fillStyle = "rgb(" + r + "," + g + "," + b + ")";
                ctx.fillRect(x, HEIGHT - barHeight, barWidth, barHeight);

                x += barWidth + 1;
            }
        }

        audio.play();
        renderFrame();
    };
});